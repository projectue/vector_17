﻿
#include <iostream>
#include "math.h"

class Vector
{ 
private:

    double x;
    double y;
    double z;

public:

    Vector(): x(5), y(3), z(1)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}


    double show()
        {
        double V = x * x + y * y + z * z;
        return sqrt(V);
        }
    
};

int main()
{
    Vector V;
    V.show();
   
    std::cout << V.show();
}
    

